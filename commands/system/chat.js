const { MessageEmbed } = require('discord.js');
const axios = require('axios');
const jsonfile = require('jsonfile');
const util = require('util');
const readFile = util.promisify(jsonfile.readFile);
let sessionID;
let isProcessing = false; // Semaphore variable

// TODO: Chose an http client and stick with it. 
// Axios Vs Unirest - TBD

module.exports = {
    name: 'chat',
    description: 'Chat with Alpaca using rAI',
    options: [{
        "name": "query",
        "description": "The thing you want to ask",
        "required": true,
        "type": 3 // 6 is type USER
    }],

    run: async (client, interaction) => {
        const file = './cache/' + interaction.user.id;
        let chatToSend = interaction.options._hoistedOptions[0].value;
        // Check if another request is already being processed
        if (isProcessing) {
            interaction.followUp({ content: 'Sorry, another request is already being processed. Please try again in a few minutes.', ephemeral: true });
            return;
        }
        // Set the semaphore to true
        isProcessing = true;

        try {
            const session = await readFile(file);

            sessionID = session.id;

            if (!sessionID) {
                console.log("No Session!");
                isProcessing = false;
                return;
            }

            let prompt = interaction.options._hoistedOptions[0].value.replace(" ", "+");
            const headers = {
                'authority': process.env.PUBLIC_URL,
                'accept': 'text',
            };
            console.log(session);
            const response = await axios.post(`http://${process.env.INTERNAL_IP}:${process.env.SERGE_PORT}/api/chat/${sessionID}/question?prompt=${prompt}`, {
                headers
            });

            console.log(`Prompt: ${prompt.replace("+", " ")}\nResponse: ${response.data.answer}`);

            if (!response.data.answer) throw new Error("Did not receive a reply. API error?")


            interaction.editReply(response.data.answer);
        } catch (err) {
            console.error(err);
            interaction.editReply('Please create a session using /create-session.');
        } finally {
            // Set the semaphore to false
            isProcessing = false;
        }
    },
};
