const { EmbedBuilder } = require('discord.js');

module.exports = {
  name: "about",
  description: "Info about the bot",
  run: async (client, interaction) => {
    const specsFields = [
      {
        name: "Processor",
        value: "Intel i7-1065G7 (8) @ 3.900GHz"
      },
      {
        name: "Memory",
        value: "11 GB RAM"
      },
      {
        name: "Chat Threads",
        value: "7"
      },
      {
        name: "Memory Speed",
        value: "3733 MT/s"
      },
      {
        name: "Other",
        value: "USB Liveboot\nNo VideoCard on Board!\nSingle Task Only - 256 Max Token Output"
      }
    ];
    
    const embed = new EmbedBuilder()
    .setColor("#FF0000")
      .setTitle("About rAI")
      .setDescription(`Latency : ${client.ws.ping}ms\n\nrAI is a bot managed by \`snxraven#8205\` \nRunning GPT4ALL and LLama 7B/7B-native.`)
      .addFields(specsFields)
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
    
    interaction.followUp({ embeds: [embed] });
  },
};
