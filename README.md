# rAI - Serge Discord Bot

This is a discord client written in DiscordJS v14 based on DiscordV10

This bot is slash command based only and currently supports creating a session or "chat room" per user. 

These sessions can be generated an unlimited amount of times.

The data on which roomID is active or which user is stored within a ./cache directory. 

The cache file is named the DiscordID of the user creating the session. Session caches are overwritten on each request.

This bot also has the ability to edit start up paramaters for the room itself such as the Model, init-prompt, temperature and the repeat-penalty.

The bot will run a POST request for a static reply from serge, message response times will depend on the full generation window of your Serge instance.

Message intents are NOT supported in this bot, this is due to the verification that Discord is enabling.

# Commands

`/create-session` - create a new chat session Optional Options: (Model, init-prompt, temperature and the repeat-penalty)

`/view-session-id` - View your current session id

`/view-session-history` - View your current session id

`/chat` - Chat with the bot

# General Usage

1) Use ```npm i ```

2) Create a .env file ``` cp default.env .env```

3) Edit .env ```nano .env```

4) Go into https://discord.com/developers/applications and enable Privileged Intents 

(This may not be needed at this point of you want to try without.)

5) Run the bot ```node rai.js```


Want to make this better? Issue a pull request!
